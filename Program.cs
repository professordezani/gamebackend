using GameBackend.Models;
using Microsoft.EntityFrameworkCore;

namespace GameBackend;

public class Program
{
    public static void Main(string[] args)
    {
        WebApplicationBuilder builder = WebApplication.CreateBuilder(args);

        builder.Services.AddCors();

        // adicionar middlewares (services):
        builder.Services.AddControllers();

        builder.Services.AddCors(option =>
            option.AddPolicy(name: "MyAllowSpecificOrigins",
             builder =>
             {
                 builder
                     .AllowAnyMethod()
                     .AllowAnyHeader()
                     .AllowAnyOrigin();
             }));

        // singleton ou transient

        // string strConn = builder.Configuration.GetConnectionString("BDFatec");
        // builder.Services.AddDbContext<DBGame>(option => option.UseSqlServer(strConn));

        builder.Services.AddDbContext<DBGame>(option => option.UseInMemoryDatabase("db"));

        // Adicionar o middleware Swagger:
        builder.Services.AddEndpointsApiExplorer();
        builder.Services.AddSwaggerGen();

        WebApplication app = builder.Build();

        if (app.Environment.IsDevelopment())
        {
            app.UseSwagger();
            app.UseSwaggerUI();
        }

        app.UseCors("MyAllowSpecificOrigins");

        // usa o middleware (adiciona no pipeline de execução):
        app.MapControllers();

        app.Run();
    }
}