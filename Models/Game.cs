namespace GameBackend.Models;

public class Game
{
   public int GameId { get; set; }
   public int UserId { get; set; }
   public string Name { get; set; }
   public bool  Status { get; set; }
   public User? User { get; set; }
   public List<Quiz>? Quizzes { get; set; }
}