using System.ComponentModel.DataAnnotations;

namespace GameBackend.Models;

public class User
{
    // propriedades automáticas:
    public int UserId { get; set; }

    [MaxLength(60)]
    public string Name { get; set; }
    public string Email { get; set; }
    public string Password { get; set; }
}