using Microsoft.EntityFrameworkCore;

namespace GameBackend.Models;

public class DBGame: DbContext
{
    public DBGame(DbContextOptions<DBGame> options) : base(options)
    {
    }
    
    public DbSet<User> Users { get; set; }
    public DbSet<Game> Games { get; set; }
    public DbSet<Quiz> Quizzes { get; set; }

    // public DbSet<Pessoa> Pessoas { get; set; }
    // public DbSet<Cliente> Clientes { get; set; }
    // public DbSet<Colaborador> Colaboradores { get; set; }

}