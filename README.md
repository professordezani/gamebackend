# Projeto GameBackend

## Bibliotecas utilizadas

### Swagger

`dotnet add package Swashbuckle.AspNetCore --version 6.4.0`

### Entity Framework Core

(1) Instalar o CLI do EF

`dotnet tool install --global dotnet-ef`

(2) Adicionar os pacotes do Entity Framework:

`dotnet add package Microsoft.EntityFrameworkCore.Design`

`dotnet add package Microsoft.EntityFrameworkCore.InMemory`

(3) Executar e ver documentação:

`dotnet ef`

### Entity Framework Core com SQL Server

(1) Adiciona o pacote

`dotnet add package Microsoft.EntityFrameworkCore.SqlServer`

(2) Adiciona a string de conexão nos arquivos appsettings

```
  "ConnectionStrings": {
    "BDFatec": "Server=localhost;Database=DBGame;Trusted_Connection=true"
  }
```

(3) Altera a injeção de dependência do EF para usar SQL Server no Program.cs

```
string strConn = builder.Configuration.GetConnectionString("BDFatec");

builder.Services.AddDbContext<DBGame>(option => option.UseSqlServer(strConn));
```

(4) Executa os comandos para adicionar migrações

`dotnet ef migrations add "Migration Name"`

(5) Caso deseje, execute o comando para ver o SQL gerado

`dotnet ef migrations script > Script.sql`

(6) Atualize o banco de dados

`dotnet ef database update`


## Referências:

https://learn.microsoft.com/en-us/ef/core/cli/dotnet

https://learn.microsoft.com/en-us/ef/ef6/fundamentals/working-with-dbcontext

https://learn.microsoft.com/en-us/ef/ef6/fundamentals/connection-management

https://learn.microsoft.com/en-us/ef/core/dbcontext-configuration/


## Changelog


2022-09-13

- Criamos nossa primeira API com os métodos GET e POST.

2022-10-04

- Adicionamos o entity framework ao projeto (InMemoryDatabase)

2022-10-04

- Alteramos o entity framework para persistir os dados no SQL Server