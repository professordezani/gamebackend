using Microsoft.AspNetCore.Mvc;
using GameBackend.Models;
using Microsoft.EntityFrameworkCore;

namespace GameBackend.Controllers;

[ApiController]
[Route("game")]
public class GameController: ControllerBase
{
    private DBGame db;

    public GameController(DBGame db)
    {
        this.db = db;
    }

    [HttpGet]
    public ActionResult Read()
    {        
        // SELECT * FROM Games
        return Ok(db.Games.Include(g => g.User).ToList());
    }

    [HttpGet]
    [Route("{id}")]
    public ActionResult Read(int id)
    {
        Game? _game = db.Games.Find(id);

        if(_game == null)
            return NotFound();

        return Ok(_game);
    }

    [HttpPost]
    public ActionResult Create(Game game)
    {
        // INSERT INTO Game VALUES (...)
        db.Games.Add(game);
        db.SaveChanges();

        // processar o que eu quiser.
        return Created(game.GameId.ToString(), game);
    }

    [HttpDelete]
    [Route("{id}")]
    public ActionResult Delete(int id)
    {
        Game? game = db.Games.Find(id);

        if(game == null)
            return NotFound();

        db.Games.Remove(game);
        db.SaveChanges();

        return Ok();
    }

    [HttpPut]
    [Route("{id}")]
    public ActionResult Update(int id, Game game)
    {
        Game? _game = db.Games.Find(id);
        if(_game == null)
            return NotFound();

        // UPDATE Game SET Name = "", Status = true WHERE id = 1
        _game.Name = game.Name;
        _game.Status = game.Status;

        db.SaveChanges();
        return Ok();
    }
}