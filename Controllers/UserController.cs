using Microsoft.AspNetCore.Mvc;
using GameBackend.Models;

namespace GameBackend.Controllers;

[ApiController]
[Route("user")]
public class UserController: ControllerBase
{
    private DBGame db;

    public UserController(DBGame db)
    {
        this.db = db;
    }

    [HttpGet]
    public ActionResult Read()
    {        
        return Ok(db.Users.ToList());
    }

    [HttpPost]
    public ActionResult Create(User user)
    {
        // INSERT INTO Game VALUES (...)
        db.Users.Add(user);
        db.SaveChanges();

        // processar o que eu quiser.
        return Created(user.UserId.ToString(), user);
    }
}