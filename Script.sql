Build started...
Build succeeded.
The Entity Framework tools version '6.0.9' is older than that of the runtime '6.0.10'. Update the tools for the latest features and bug fixes. See https://aka.ms/AAc1fbw for more information.
info: Microsoft.EntityFrameworkCore.Infrastructure[10403]
      Entity Framework Core 6.0.10 initialized 'DBGame' using provider 'Microsoft.EntityFrameworkCore.SqlServer:6.0.10' with options: None
IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;
GO

BEGIN TRANSACTION;
GO

CREATE TABLE [Users] (
    [UserId] int NOT NULL IDENTITY,
    [Name] nvarchar(max) NOT NULL,
    [Email] nvarchar(max) NOT NULL,
    [Password] nvarchar(max) NOT NULL,
    CONSTRAINT [PK_Users] PRIMARY KEY ([UserId])
);
GO

CREATE TABLE [Games] (
    [GameId] int NOT NULL IDENTITY,
    [UserId] int NOT NULL,
    [Name] nvarchar(max) NOT NULL,
    [Status] bit NOT NULL,
    CONSTRAINT [PK_Games] PRIMARY KEY ([GameId]),
    CONSTRAINT [FK_Games_Users_UserId] FOREIGN KEY ([UserId]) REFERENCES [Users] ([UserId]) ON DELETE CASCADE
);
GO

CREATE TABLE [Quizzes] (
    [QuizId] int NOT NULL IDENTITY,
    [GameId] int NOT NULL,
    [Question] nvarchar(max) NOT NULL,
    [Option1] nvarchar(max) NOT NULL,
    [Option2] nvarchar(max) NOT NULL,
    [Option3] nvarchar(max) NOT NULL,
    [Option4] nvarchar(max) NOT NULL,
    [Correct] int NOT NULL,
    CONSTRAINT [PK_Quizzes] PRIMARY KEY ([QuizId]),
    CONSTRAINT [FK_Quizzes_Games_GameId] FOREIGN KEY ([GameId]) REFERENCES [Games] ([GameId]) ON DELETE CASCADE
);
GO

CREATE INDEX [IX_Games_UserId] ON [Games] ([UserId]);
GO

CREATE INDEX [IX_Quizzes_GameId] ON [Quizzes] ([GameId]);
GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20221018191106_Primeira Vers├úo', N'6.0.10');
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

DECLARE @var0 sysname;
SELECT @var0 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Users]') AND [c].[name] = N'Name');
IF @var0 IS NOT NULL EXEC(N'ALTER TABLE [Users] DROP CONSTRAINT [' + @var0 + '];');
ALTER TABLE [Users] ALTER COLUMN [Name] nvarchar(60) NOT NULL;
GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20221018193746_AdicionaMaxLength', N'6.0.10');
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

DECLARE @var1 sysname;
SELECT @var1 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Quizzes]') AND [c].[name] = N'Option4');
IF @var1 IS NOT NULL EXEC(N'ALTER TABLE [Quizzes] DROP CONSTRAINT [' + @var1 + '];');
ALTER TABLE [Quizzes] ALTER COLUMN [Option4] nvarchar(max) NULL;
GO

DECLARE @var2 sysname;
SELECT @var2 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Quizzes]') AND [c].[name] = N'Option3');
IF @var2 IS NOT NULL EXEC(N'ALTER TABLE [Quizzes] DROP CONSTRAINT [' + @var2 + '];');
ALTER TABLE [Quizzes] ALTER COLUMN [Option3] nvarchar(max) NULL;
GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20221018203232_OptionNull', N'6.0.10');
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

CREATE TABLE [Pessoas] (
    [PessoaId] int NOT NULL IDENTITY,
    [Nome] nvarchar(max) NOT NULL,
    CONSTRAINT [PK_Pessoas] PRIMARY KEY ([PessoaId])
);
GO

CREATE TABLE [Clientes] (
    [PessoaId] int NOT NULL,
    [Documento] nvarchar(max) NOT NULL,
    CONSTRAINT [PK_Clientes] PRIMARY KEY ([PessoaId]),
    CONSTRAINT [FK_Clientes_Pessoas_PessoaId] FOREIGN KEY ([PessoaId]) REFERENCES [Pessoas] ([PessoaId])
);
GO

CREATE TABLE [Colaboradores] (
    [PessoaId] int NOT NULL,
    [Endereco] nvarchar(max) NOT NULL,
    CONSTRAINT [PK_Colaboradores] PRIMARY KEY ([PessoaId]),
    CONSTRAINT [FK_Colaboradores_Pessoas_PessoaId] FOREIGN KEY ([PessoaId]) REFERENCES [Pessoas] ([PessoaId])
);
GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20221025193827_heranca', N'6.0.10');
GO

COMMIT;
GO


